Example Projects Built with Vagga
=================================

To run vagga in gitlab CI's docker infrastructure you may just use
a base image with vagga. Then the whole power of vagga is unleashed:

```yaml
image: tailhook/vagga:v0.7.2-93-g0884e15

test:
  script:
  - vagga test
```

